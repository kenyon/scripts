#!/bin/bash

set -o errexit

grep --quiet --fixed-strings --regexp='ufv.custom.certs.enable=true' /usr/lib/unifi-video/data/system.properties || echo 'ufv.custom.certs.enable=true' >> /usr/lib/unifi-video/data/system.properties
mkdir --parents /var/lib/unifi-video/certificates
chown --recursive unifi-video: /var/lib/unifi-video/certificates
rm --force /var/lib/unifi-video/{keystore,ufv-truststore} /usr/lib/unifi-video/conf/evostream/server.{crt,key}
openssl x509 -in /etc/letsencrypt/live/unifi.kenyonralph.com/fullchain.pem -inform PEM -out /var/lib/unifi-video/certificates/ufv-server.cert.der -outform DER
openssl pkcs8 -topk8 -inform PEM -outform DER -in /etc/letsencrypt/live/unifi.kenyonralph.com/privkey.pem -out /var/lib/unifi-video/certificates/ufv-server.key.der -nocrypt
chown --recursive unifi-video: /var/lib/unifi-video/certificates
